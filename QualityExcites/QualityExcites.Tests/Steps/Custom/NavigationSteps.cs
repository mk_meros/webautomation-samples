﻿using QualityExcites.Tests.Common;
using QualityExcites.Tests.Pages;
using TechTalk.SpecFlow;

namespace QualityExcites.Tests.Steps
{
    [Binding]
    public class NavigationSteps : SpecFlowTest
    {
        public NavigationSteps(ScenarioContext scenarioContext) 
            : base(scenarioContext)
        {
        }

        public QualityExcitesPage Page
        {
            get
            {
                return this.ATDriver.Get<QualityExcitesPage>();
            }
        }

        [Given(@"User navigates to '(.+)' page")]
        [When(@"User navigates to '(.+)' page")]
        public void GivenIDoSomething(string pageName)
        {
            var menuLink = this.Page.MenuLink.With(pageName);
            menuLink.Assert.Is.Displayed();
            menuLink.Perform.Click();
        }

        [Then(@"'(.*)' page is displayed")]
        public void ThenPageIsDisplayed(string pageName)
        {
            var header = this.Page.Header;
            header.Assert.Is.Displayed();
            header.Assert.Has.Text(pageName.ToUpper());
        }

        [When(@"User clicks on submit button")]
        public void WhenUserClicksOnSubmitButton()
        {
            this.Page.SubmitButton.Perform.Click();
        }

        [When(@"User clicks on '(.*)' button")]
        public void WhenUserClicksOnButton(string buttonName)
        {
            this.Page.ButtonByText.With(buttonName).Perform.Click();
        }
    }
}