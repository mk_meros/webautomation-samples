﻿using TechTalk.SpecFlow;
using WebAutomation.Core.WebObjects.WebComponents;
using WebAutomation.Core.WebObjects.WebComponents.Value;
using QualityExcites.Tests.Common;
using QualityExcites.Tests.Pages;

namespace QualityExcites.Tests.Steps.Custom
{
    [Binding]
    public class CallForProposalsSteps : SpecFlowTest
    {
        public CallForProposalsSteps(ScenarioContext scenarioContext) 
            : base(scenarioContext)
        {
        }

        [Then(@"User is successfully registered")]
        public void ThenUserIsSuccessfullyRegistered()
        {
            this.ATDriver.Get<CallForProposalsPage>().Header.Assert.WillHave.Text("THANK YOU");
        }

        /// <summary>
        /// This kind of step can be also implemented in a generic way (to be reusable in many pages).
        /// See "User fills following fields on '(.*)' page"
        /// 
        /// For demonstration purposes, current implementation is dedicated only for "CallForProposalsPage"
        /// </summary>
        [Then(@"All fields from step (.*) are marked as invalid")]
        public void ThenAllFieldsFromStepAreMarkedAsInvalid(int step)
        {
            var page = this.ATDriver.Get<CallForProposalsPage>();

            var stepComponents = step == 1 
                ? new IWebComponent[]
                    {
                        page.Title,
                        page.Description,
                        page.Form,
                        page.Speaker.With("yes"),
                        page.Presented.With("yes")
                    }
                : new IWebComponent[]
                    {
                        page.FirstName,
                        page.Surname,
                        page.Position,
                        page.CompanyInstitution,
                        page.EmailAddress,
                        page.PhoneNumber,
                        page.Biography,
                        page.Agreement,
                        page.Captcha,
                        page.Photo
                    };

            foreach (var webComponent in stepComponents)
            {
                webComponent.Assert.Has.AttributeValue("class", "not-valid", StringType.Contains);
            }
        }
    }
}
