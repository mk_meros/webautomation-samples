﻿using TechTalk.SpecFlow;
using QualityExcites.Tests.Common;

namespace QualityExcites.Tests.Steps
{
    [Binding]
    public class ScenarioSteps : SpecFlowTest
    {
        public ScenarioSteps(ScenarioContext scenarioContext) 
            : base(scenarioContext)
        {
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            this.OpenBrowser();
            this.NavigateToHomePage();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            this.ATDriver.QuitBrowser();
        }
    }
}
