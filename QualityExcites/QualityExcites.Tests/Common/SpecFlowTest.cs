﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using TechTalk.SpecFlow;
using WebAutomation.Core;

namespace QualityExcites.Tests.Common
{
    public class SpecFlowTest
    {
        public Settings Settings { get; set; }

        public ScenarioContext ScenarioContext { get; set; }

        public IAutomationDriver ATDriver
        {
            get
            {
                return this.ScenarioContext.ContainsKey(nameof(ATDriver))
                    ? (IAutomationDriver)this.ScenarioContext[nameof(ATDriver)]
                    : null;
            }
            set
            {
                this.ScenarioContext[nameof(ATDriver)] = value;
            }
        }

        public SpecFlowTest(ScenarioContext scenarioContext)
        {
            this.ScenarioContext = scenarioContext;
            this.Settings = new Settings();

            if (this.ATDriver == null)
            {
                this.ATDriver = AutomationDriverFactory.Get();          
                this.ATDriver.Extensions.DefineClickAction<CustomClick>();
            }          
        }

        public void OpenBrowser()
        {
            switch (this.Settings.BrowserType)
            {
                case "firefox":
                    this.ATDriver.WebDriver = new FirefoxDriver();
                    break;

                case "chrome":
                    this.ATDriver.WebDriver = new ChromeDriver();
                    break;

                default:
                    throw new ArgumentException("Browser type not supported");
            }
            
            this.ATDriver.WebDriver.Manage().Window.Maximize();
        }

        public void NavigateToHomePage()
        {
            this.ATDriver.WebDriver.Navigate().GoToUrl(this.Settings.PageUrl);
        }
    }
}
