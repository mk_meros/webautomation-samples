﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using WebAutomation.Core;
using YoutubeTests.Pages;

namespace YoutubeTests
{
    [TestFixture]
    public class Tests
    {
        public IAutomationDriver ATDriver { get; set; }

        [OneTimeSetUp]
        public void BeforeAllTests()
        {
            this.ATDriver = AutomationDriverFactory.Get();
            this.ATDriver.WebDriver = new ChromeDriver();
        }

        [OneTimeTearDown]
        public void AfterAllTests()
        {
            this.ATDriver.QuitBrowser();
        }

        [TestCase("Hans Zimmer greatest hits", "The greatest hits from Hans Zimmer")]
        [TestCase("james horner hymn to the sea", "Titanic- Hymn to the sea")]
        public void OpenVideo(string input, string expectedLink)
        {     
            this.ATDriver.WebDriver.Navigate().GoToUrl("http://www.youtube.com");

            var youtubePage = this.ATDriver.Get<YoutubePage>();
            youtubePage.SearchInput.Perform.Fill(input);
            youtubePage.OkButton.Perform.Click();

            var searchResultsPage = this.ATDriver.Get<YoutubeSearchResultPage>();
            searchResultsPage.Link.With(expectedLink).Perform.Click();

            var playerPage = this.ATDriver.Get<YoutubePlayerPage>();
            playerPage.Player.Assert.Is.Displayed();  
        }
    }
}