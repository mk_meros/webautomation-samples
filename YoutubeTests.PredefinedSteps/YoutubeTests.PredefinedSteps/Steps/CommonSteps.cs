﻿using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using WebAutomation.SpecFlow;

namespace YoutubeTests.PredefinedSteps.Steps
{
    [Binding]
    public class CommonSteps : SpecFlowTestBase
    {
        public CommonSteps(ScenarioContext scenarioContext) 
            : base(scenarioContext)
        {
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            this.WebDriver = new ChromeDriver();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            this.QuitBrowser();
        }
    }
}
