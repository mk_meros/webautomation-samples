﻿using OpenQA.Selenium.Chrome;
using System.IO;
using TechTalk.SpecFlow;
using WebAutomation.Core;

namespace HowTo.SpecFlow.Steps
{
    [Binding]
    public class Common : StepsBaseClass
    {
        public Common(ScenarioContext scenarioContext) 
            : base(scenarioContext)
        {
        }

        [Given(@"I am on ""(.*)"" page")]
        public void GivenIAmOnPage(string pageName)
        {
            this.ATDriver.WebDriver = new ChromeDriver();
            this.ATDriver.WebDriver.Navigate().GoToUrl(this.GetUrl(pageName));
        }

        [AfterScenario]
        public void AfterScenario()
        {
            this.ATDriver.QuitBrowser();
        }

        protected string GetUrl(string pageName)
        {
            return Path.Combine(TestBase.AssemblyDirectory, "Resources", $"{pageName}.html");
        }
    }
}
