﻿using HowTo.SpecFlow.Pages.Approach1_2;
using TechTalk.SpecFlow;

namespace HowTo.SpecFlow.Steps.Approach1
{
    [Binding]
    class NavigationSteps : StepsBaseClass
    {
        public NavigationSteps(ScenarioContext scenarioContext) 
            : base(scenarioContext)
        {
        }

        [When(@"I click on News button in top menu")]
        public void WhenIClickOnButtonInTopMenu()
        {
            this.ATDriver.Get<MyWebsite>().MenuNewsButton.Perform.Click();
        }

        [When(@"I click on Login button in top menu")]
        public void WhenIClickOnLoginButtonInTopMenu()
        {
            this.ATDriver.Get<MyWebsite>().MenuLoginButton.Perform.Click();
        }
    }
}
