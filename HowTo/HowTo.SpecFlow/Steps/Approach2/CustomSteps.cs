﻿namespace HowTo.SpecFlow.Steps.Approach2
{
    using OpenQA.Selenium.Chrome;
    using System.IO;
    using TechTalk.SpecFlow;
    using WebAutomation.Core;
    using WebAutomation.SpecFlow;

    [Binding]
    public class CustomSteps : SpecFlowTestBase
    {
        public CustomSteps(ScenarioContext scenarioContext)
            : base(scenarioContext)
        {
        }

        [Given(@"I am on ""(.*)"" page \[approach_2\]")]
        public void GivenIAmOnPage_Approach2(string pageName)
        {
            this.WebDriver = new ChromeDriver();
            this.WebDriver.Navigate().GoToUrl(this.GetUrl(pageName));
        }

        [AfterScenario]
        public void AfterScenario()
        {
            this.QuitBrowser();
        }

        private string GetUrl(string pageName)
        {
            return Path.Combine(TestBase.AssemblyDirectory, "Resources", $"{pageName}.html");
        }
    }
}
