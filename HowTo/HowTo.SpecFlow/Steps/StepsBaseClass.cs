﻿namespace HowTo.SpecFlow.Steps
{
    using TechTalk.SpecFlow;
    using WebAutomation.Core;

    public class StepsBaseClass
    {
        public ScenarioContext ScenarioContext { get; set; }

        public IAutomationDriver ATDriver
        {
            get
            {
                return this.ScenarioContext.ContainsKey(nameof(ATDriver))
                    ? (IAutomationDriver)this.ScenarioContext[nameof(ATDriver)]
                    : null;
            }
            set
            {
                this.ScenarioContext[nameof(ATDriver)] = value;
            }
        }

        public StepsBaseClass(ScenarioContext scenarioContext)
        {
            this.ScenarioContext = scenarioContext;

            if (this.ATDriver == null)
            {
                this.ATDriver = AutomationDriverFactory.Get();
            }
        }
    }
}
