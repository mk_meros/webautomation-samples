﻿using HowTo.ExtendFramework.Common;
using NUnit.Framework;
using System;
using WebAutomation.Core;

namespace HowTo.ExtendFramework.Tests
{
    [TestFixture]
    public class FrameworkSettings
    {
        [Test]
        /// <summary>
        /// How to change the default settings used by WebAutomation framework.
        /// </summary>
        public void CustomFrameworkSettings()
        {
            // Use Settings class from WebAutomation framework.
            var atDriver = AutomationDriverFactory.Get();
            atDriver.Settings = new Settings(6000, 500, 100, 100, 6, 500);

            Assert.That(atDriver.Settings.CheckIfPresentTimeout, Is.EqualTo(TimeSpan.FromMilliseconds(6000)));
        }


        /// <summary>
        /// How to use custom implementation of settings by WebAutomation framework.
        /// </summary>
        [Test]      
        public void CustomFrameworkSettingsImplementation()
        {
            // Create custom settings class that implements ISettings interface
            var atDriver = AutomationDriverFactory.Get();
            atDriver.Settings = new CustomSettings();

            Assert.That(atDriver.Settings.CheckIfPresentTimeout, Is.EqualTo(TimeSpan.FromSeconds(10)));
        }
    }
}