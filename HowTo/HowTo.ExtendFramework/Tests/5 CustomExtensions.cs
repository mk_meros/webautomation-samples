﻿using HowTo.ExtendFramework.Common;
using HowTo.ExtendFramework.Pages;
using NUnit.Framework;

namespace HowTo.ExtendFramework.Tests
{
    [TestFixture]
    public class CustomExtensions : CustomTestBase
    {
		/// <summary>
        /// The simplest way to add a new helper method
        /// </summary>
        [Test]
		public void ExtensionMethods()
        {
            var page = this.ATDriver.Get<MyWebsite>();

            // Instead of this:
            var loginBUtton = page.MenuHomeButton;
            loginBUtton.Assert.Is.Present();
            loginBUtton.Perform.Click();

            // You can use extension method:
            page.MenuHomeButton.AssertPresentAndPerform().Click();
        }

		[Test]
		public void CustomImplementation()
        {
            this.ATDriver.Extensions.RegisterActivity<CustomActivities.SaveHtmlSourcesActivity>();
            this.ATDriver.Extensions.RegisterActivity<CustomActivities.PerformWhenReadyActivity>();
            // Replace implementation of IWebComponent (do not recommend without a really good reason)
            this.ATDriver.Extensions.DefineWebComponent<CustomWebComponent>();

            var page = this.ATDriver.Get<MyWebsite>();
            // DoSomething() method from  CustomWebComponent class will be invoked for each WebComponent in MyWebsite
        }
    }
}