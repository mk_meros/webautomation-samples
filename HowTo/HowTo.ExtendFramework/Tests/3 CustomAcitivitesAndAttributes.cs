﻿using HowTo.ExtendFramework.CustomActivities;
using HowTo.ExtendFramework.Pages;
using NUnit.Framework;

namespace HowTo.ExtendFramework.Tests
{
    [TestFixture]
    public class HowToUseCustomAcitivitesAndAttributes : CustomTestBase
    {
        public HowToUseCustomAcitivitesAndAttributes()
        {
            this.ATDriver.Extensions.RegisterActivity<TurnOnMailServerActivity>();
        }

        /// <summary>
        /// Custom activities can be executed also for the specified container / component or an action.
        /// 
        /// Steps:
        /// - Create custom attribute
        /// - Create custom activity with RequiredAttributeType equal to the attribute type.
        /// - Register the activity
        /// </summary>
        [Test]
        public void WebContainerActivityWihAttribute()
        {
            var page = this.ATDriver.Get<MyWebsite>();             
            page.MenuLoginButton.Perform.Click();

            page = this.ATDriver.Get<LoginPage>();
            // TurnOnMailServerActivity will be executed
        }
    }
}
