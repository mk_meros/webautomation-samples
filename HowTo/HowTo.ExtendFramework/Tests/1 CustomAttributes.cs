﻿using HowTo.ExtendFramework.Pages;
using NUnit.Framework;
using WebAutomation.Core.WebObjects.WebComponents;

namespace HowTo.ExtendFramework.Tests
{
    [TestFixture]
    public class HowToUseCustomAttributesTest : CustomTestBase
    {
        /// <summary>
        /// Custom attributes can be used to:
        /// - store additional information about a container / component (see example below)
        /// - execute custom activities for a container / component / action (see 3 CustomAcitivitesAndAttributes.cs)
        /// 
        /// Steps:
        /// - Crete a class for your custom attribute 
        ///   (inherit from WebComponentAttribute or WebContainerAttribute)
        /// - Define the namespace of you custom attribute in .tt file
        ///   string additionalUsing = "HowTo.ExtendFramework.CustomAttributes";
        /// - Use the attribute in the XML file   
        /// </summary>
        [Test]
        public void WebComponentAttribute()
        {
            var page = this.ATDriver.Get<MyWebsite>();
            page.MenuLoginButton.Perform.Click();

            var loginPage = this.ATDriver.Get<LoginPage>();

            // Here I know that this element is a textbox 
            // ... so I can fill it with a value
            var user = loginPage.UserTextbox;

            // But when I pass this textbox to a different method
            // ... then how this method will know what to do?
            this.DoSomething(user, "admin");

            // ... and so on
            this.DoSomething(loginPage.PasswordTextbox, "pass");
            this.DoSomething(loginPage.LoginButton);
        }

        /// <summary>
        /// Below example shows ability to retrieve information about attributes in XML.
        /// This can be useful when creating generic methods / BDD tests.
        /// </summary>
        public void DoSomething(IWebComponent webComponent, string value = null)
        {
            // I have access to all attributes defined in the XML (including my custom "type"!)
            // So base on that I can perform a desired action.

            webComponent.Assert.WillBe.Displayed();

            switch (webComponent.Properties["type"])
            {
                case "button":
                    webComponent.Assert.Is.Enabled();
                    webComponent.Perform.Click();
                    break;

                case "textbox":
                    webComponent.Perform.Fill(value);
                    break;

                case "text":
                    webComponent.Assert.Has.Text(value);
                    break;
            }
        }
    }
}
