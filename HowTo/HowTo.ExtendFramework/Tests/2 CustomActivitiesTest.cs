﻿using HowTo.ExtendFramework.CustomActivities;
using HowTo.ExtendFramework.Pages;
using NUnit.Framework;

namespace HowTo.ExtendFramework.Tests
{
    [TestFixture]
    public class HowToUseCustomActivitiesTest : CustomTestBase
    {
        public HowToUseCustomActivitiesTest()
        {
            // Register custom activities:
            this.ATDriver.Extensions.RegisterActivity<SaveHtmlSourcesActivity>();
            this.ATDriver.Extensions.RegisterActivity<PerformWhenReadyActivity>();
        }

        /// <summary>
        /// Custom activities can be used to execute additional action when loading a component / container or performing an action on component.
        /// It can be helpful if you want to avoid invoking the same action  many times manually.
        /// 
        /// Steps:
        /// - Crete a class for your custom activity 
        ///   (implement IWebContainerActivity, IWebComponentActivity or IOnActionActivity)
        /// - Register the activity
        /// </summary>
        [Test]
        public void WebContainerAndOnActionActivities()
        {
            var page = this.ATDriver.Get<MyWebsite>();
            // SaveHtmlSourcesActivity will be executed

            page.MenuLoginButton.Perform.Click();
            // PerformWhenReadyActivity will be executed before "Click"

            page = this.ATDriver.Get<LoginPage>();
            // SaveHtmlSourcesActivity will be executed
        }
    }
}
