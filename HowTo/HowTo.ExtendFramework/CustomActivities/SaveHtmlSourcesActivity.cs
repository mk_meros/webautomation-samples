﻿using System;
using OpenQA.Selenium;
using WebAutomation.Core.WebObjects.WebContainer.Activities;
using System.IO;
using WebAutomation.Core;

namespace HowTo.ExtendFramework.CustomActivities
{
    /// <summary>
    /// Save html sources of each page I visit.
    /// </summary>
    public class SaveHtmlSourcesActivity : IWebContainerActivity
    {
        /// <summary>
        /// The type is used to indicate for which containers the activity should be executed.
        /// </summary>
        public Type RequiredAttributeType
        {
            get
            {
                // If the type is not specified (null value), the activity will be executed for each container.
                return null;
            }
        }

        public void Perform(object webContainer, IWebDriver webDriver, string attributeValue = null)
        {
            // Your custom logic here...
   
            string pageSource = webDriver.PageSource;
            string filename = string.Format("{0}.txt", webContainer.GetType().Name);
            string filePath = Path.Combine(TestBase.AssemblyDirectory, filename);
            File.WriteAllText(filePath, pageSource);
        }
    }
}
