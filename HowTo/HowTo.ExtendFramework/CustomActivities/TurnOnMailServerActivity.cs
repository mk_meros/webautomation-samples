﻿using HowTo.ExtendFramework.CustomAttributes;
using OpenQA.Selenium;
using System;
using WebAutomation.Core.WebObjects.WebContainer.Activities;

namespace HowTo.ExtendFramework.CustomActivities
{
    /// <summary>
    /// Rurn on Mail server.
    /// </summary>
    public class TurnOnMailServerActivity : IWebContainerActivity
    {
        public Type RequiredAttributeType
        {
            get
            {
                // When I visit a page with this attribute,
                // I want to ensure that the mail server is running.
                return typeof(RequireMailServerAttribute);
            }
        }

        public void Perform(object webContainer, IWebDriver webDriver, string attributeValue = null)
        {
            // Your custom logic here...

            Console.WriteLine("Mail server turned on...");
        }
    }
}
