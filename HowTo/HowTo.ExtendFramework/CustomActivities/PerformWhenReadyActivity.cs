﻿using System;
using WebAutomation.Core.WebObjects.WebComponents.Activities;
using WebAutomation.Core.WebObjects.WebComponents.WebElementProviders;

namespace HowTo.ExtendFramework.CustomActivities
{
    public class PerformWhenReadyActivity : IOnActionActivity
    {
        /// <summary>
        /// If the activity uses an attribute, the value of attribute (from XML) will be stored here.
        /// </summary>
        public string AttributeValue { get; set; }

        public ExecutionType ExecutionType
        {
            get
            {
                // Activity will be executed before the action.
                return ExecutionType.Before;
            }
        }

        public Type RequiredAttributeType
        {
            get
            {
                // If the type is not specified (null value), the activity will be executed for each WebComponent action.
                return null;
            }
        }

        public void Perform(IWebElementProvider webElementProvider)
        {
            var webDriver = webElementProvider.WebDriver;

            // Your custom logic here..
            // for example, wait for angular
        }
    }
}
