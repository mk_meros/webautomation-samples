﻿using HowTo.ExtendFramework.CustomActivities;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System.IO;
using WebAutomation.Core;

namespace HowTo.ExtendFramework
{
    /// <summary>
    /// My custom "base" class for all tests.
    /// </summary>
    public class CustomTestBase
    {
        public IAutomationDriver ATDriver { get; set; }

        public CustomTestBase()
        {
            this.ATDriver = AutomationDriverFactory.Get();

            // Here you can register your custom activities or implementation of the framework classes.

            // Custom implementations:
            this.ATDriver.Logger = new NUnit3Logger();
        }

        protected string HomePagePath
        {
            get
            {
                return Path.Combine(TestBase.AssemblyDirectory, "Resources", "Home.html");
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.ATDriver.WebDriver = new ChromeDriver();
            this.ATDriver.WebDriver.Navigate().GoToUrl(this.HomePagePath);
        }

        [TearDown]
        public void TearDown()
        {
            this.ATDriver.QuitBrowser();
        }
    }
}
