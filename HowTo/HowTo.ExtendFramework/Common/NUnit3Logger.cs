﻿using NUnit.Framework;
using WebAutomation.Core.Logger;

namespace HowTo.ExtendFramework
{
    /// <summary>
    /// In order to save logs from the framework to the NUnit3 report, a custom logger should be created
    /// </summary>
    public class NUnit3Logger : ILogger
    {
        public void Debug(string message)
        {
            TestContext.Write(message);
        }

        public void Error(string message)
        {
            TestContext.Write(message);
        }

        public void Info(string message)
        {
            TestContext.Write(message);
        }

        public void Warn(string message)
        {
            TestContext.Write(message);
        }
    }
}
