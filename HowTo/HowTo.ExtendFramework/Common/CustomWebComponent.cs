﻿using Microsoft.Practices.Unity;
using WebAutomation.Core.Logger;
using WebAutomation.Core.WebObjects.WebComponents;
using WebAutomation.Core.WebObjects.WebComponents.Actions;
using WebAutomation.Core.WebObjects.WebComponents.States;
using WebAutomation.Core.WebObjects.WebComponents.Value;

namespace HowTo.ExtendFramework.Common
{
    /// <summary>
    /// Example how to create a custom implementation of IWebComponent based on WebComponent.
    /// Please use only as a last resort if there is no other way.
    /// </summary>
    public class CustomWebComponent : WebComponent, IWebComponent
    {
        public CustomWebComponent(
            IWebComponentAssert assert, 
            [Dependency("stateIs")] IWebComponentCheckState stateIs, 
            [Dependency("stateWillBe")] IWebComponentCheckState stateWillBe, 
            [Dependency("valueHas")] IWebComponentCheckValue valueHas, 
            [Dependency("valueWillHave")] IWebComponentCheckValue valueWillHave, 
            [Dependency("actionPerform")] IWebComponentActions actionPerform, 
            [Dependency("actionPerformIfExists")] IWebComponentActions actionPerformIfExists,
            ILogger logger) 
            : base(assert, stateIs, stateWillBe, valueHas, valueWillHave, actionPerform, actionPerformIfExists, logger)
        {
            this.DoSomething();
        }


        public void DoSomething()
        {
            // Example extension
            // TODO..
        }
    }
}
