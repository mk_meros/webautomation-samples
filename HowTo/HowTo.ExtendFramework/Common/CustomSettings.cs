﻿using System;
using WebAutomation.Core;

namespace HowTo.ExtendFramework.Common
{
    public class CustomSettings : ISettings
    {
        public int BottomScrollMargin
        {
            get
            {
                return 0;
            }
        }

        public TimeSpan CheckIfNotPresentTimeout
        {
            get
            {
                return TimeSpan.FromMilliseconds(100);
            }
        }

        public TimeSpan CheckIfPresentTimeout
        {
            get
            {
                return TimeSpan.FromSeconds(10);
            }
        }

        public int TopScrollMargin
        {
            get
            {
                return 0;
            }
        }

        public int WaitForExpectedResultAttempts
        {
            get
            {
                return 4;
            }
        }

        public TimeSpan WaitForExpectedResultsSleepTime
        {
            get
            {
                return TimeSpan.FromMilliseconds(500);
            }
        }
    }
}
