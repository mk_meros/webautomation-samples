﻿namespace HowTo.ExtendFramework.Common
{
    using WebAutomation.Core.WebObjects.WebComponents;
    using WebAutomation.Core.WebObjects.WebComponents.Actions;

    public static class IWebComponentExtensions
    {
        public static IWebComponentActions AssertPresentAndPerform(this IWebComponent webComponent)
        {
            webComponent.Assert.WillBe.Present();
            return webComponent.Perform;
        }
    }
}
