﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System.IO;
using WebAutomation.Core;

namespace HowTo.XML.Tests
{
    public class CustomTestBase
    {
        public IAutomationDriver ATDriver { get; set; }

        public CustomTestBase()
        {
            this.ATDriver = AutomationDriverFactory.Get();
            this.ATDriver.Logger = new NUnit3Logger();
        }

        protected string HomePagePath
        {
            get
            {
                return Path.Combine(TestBase.AssemblyDirectory, "Resources", "Home.html");
            }
        }

        [OneTimeSetUp]
        public void BeforeAllTests()
        {
            this.ATDriver.WebDriver = new ChromeDriver();
        }

        [OneTimeTearDown]
        public void AfterAllTests()
        {
            this.ATDriver.QuitBrowser();
        }

        [SetUp]
        public void BeforeTest()
        {
            this.ATDriver.Logger.Info($"Start test: {TestContext.CurrentContext.Test.Name}");     
            this.ATDriver.WebDriver.Navigate().GoToUrl(this.HomePagePath);
        }

        [TearDown]
        public void AfterTest()
        {
            this.ATDriver.Logger.Info($"Test result: {TestContext.CurrentContext.Result.Outcome.Status}");
            this.ATDriver.Logger.Info("================================\n");
        }
    }
}