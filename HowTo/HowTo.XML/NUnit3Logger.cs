﻿using NUnit.Framework;
using WebAutomation.Core.Logger;

namespace HowTo.XML
{
    public class NUnit3Logger : ILogger
    {
        public void Debug(string message)
        {
            TestContext.Write(message);
        }

        public void Error(string message)
        {
            TestContext.Write(message);
        }

        public void Info(string message)
        {
            TestContext.Write(message);
        }

        public void Warn(string message)
        {
            TestContext.Write(message);
        }
    }
}
