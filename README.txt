Repository contains sample projects which use WebAutomation framework.

HowTo:
- HowTo.XML - demonstrates how to create the correct XML for WebAutomation.
- HowTo.SpecFlow - demonstrates possible approaches of using WebAutomation with SpecFlow
- HowTo.ExtendFramework - demonstrates how to create custom attributes / activities

QualityExcites:
- Sample test project for Quality Excites (https://qualityexcites.pl)
- SpecFlow + WebAutomation

YoutubeTests
- Simple tests for youtube using the WebAutomation framework.

YoutubeTests.PredefinedSteps
- SpecFlow tests using only predefined steps from WebAutomation.GenericSteps.